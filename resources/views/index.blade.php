<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>

    <!-- Google Api  -->
    <!--  https://maps.googleapis.com/maps/api/js?libraries=places&key=YOUR_KRY -->
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAKBHCRT-dgthjiODWPGrGem1tkfcW3oOk">
        </script>

    <!-- Search input -->
    <div id="form-group">
        <input id="search_input" type="textbox" value="Bang sue" form="col-auto" />
        <input id="submit" type="button" value="Search" class="btn btn-primary" />
    </div>

    <!-- Restaurant -->
    <div id="table">
        <table class="table">
            <tr id="restaurant" class="restaurant"></tr>
        </table>
    </div>

    <!-- GoogleMap -->
    <div id="map"></div>

    <!-- JavaScript -->
    <script>

        try {

            var map // ตัวแปรเก็บข้อมูลแผนที่
            var infowindow // ตัวแปรเก็บชื่อสถานที่

            var request // ตัวแปรเก็บสถานที่
            var service // เตัวแปรเก็บสถานที่ในรอบๆรัศมีวงกลม
            var markers = [] // ตัวแปร พิกัด

            // AutoComplete
            var searchInput = 'search_input';
            $(document).ready(function () {
                var autocomplete;
                autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                    types: ['geocode'],
                });
            });

            // Default coordinates
            function initialize() {
                var center = new google.maps.LatLng(13.824986, 100.530521)
                map = new google.maps.Map(document.getElementById('map'), {
                    center: center,
                    /*
                1: World
                5: Landmass/continent
                10: City
                15: Streets
                20: Buildings
                0-21
                 */
                    zoom: 13,
                    /*
                Map Type
                ROADMAP (แสดงถนนปกติ, เป็นค่า Default แบบ 2D หรือ 2 มิติ)
                SATELLITE (ภาพจากดาวเทียม)
                HYBRID (แบบปกติผสมดาวเทียม)
                TERRAIN (แบบภาพภูมิศาสตร์)
                */
                    // mapTypeId: google.maps.MapTypeId.HYBRID
                })

                request = {
                    location: center,
                    radius: 8047,   /* หน่วยเป็นเมตร */
                    types: ['restaurant'] // กำหนดให้ หาเฉพาะ Restaurant
                    // https://developers.google.com/places/web-service/search
                }
                // เรียกใช้ api ชื่อสถานที่
                infowindow = new google.maps.InfoWindow()

                // เรียกใช้ api รายละเอียดสถานที่
                service = new google.maps.places.PlacesService(map)

                // เรียกฟังก์ชัน ค้นหารัศมี
                service.nearbySearch(request, callback)

                // เมื่อคลิ๊กขวา จะเป็นการ Clear พิกัดเดิม
                google.maps.event.addListener(map, 'rightclick', function (event) {
                    map.setCenter(event.latLng)
                    clearResults(markers)

                    var request = {
                        location: event.latLng,
                        radius: 8047, /*หน่วยเป็นเมตร*/
                        types: ['restaurant']
                    }
                    service.nearbySearch(request, callback)
                })

                // ฟังก์ชัน Serach สถานที่หลังจาก กดปุ่น Search
                var geocoder = new google.maps.Geocoder();
                document
                    .getElementById("submit")
                    .addEventListener("click", function () {
                        geocodeAddress(geocoder, map);
                    });

            }

            // function ค้นหา
            function geocodeAddress(geocoder, resultsMap) {

                var address = document.getElementById("search_input").value;
                geocoder.geocode({
                    address: address
                },

                    // function สร้าง marker จาก Search
                    function (results, status) {
                        if (status === "OK") {
                            resultsMap.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: resultsMap,
                                position: results[0].geometry.location
                            });

                        } else {
                            alert(
                                "Geocode was not successful for the following reason: " +
                                status
                            );
                        }
                    }
                );
            }

            // function ดึงข้อมูลร้านอาหาร ตำแหน่ง และพิกัด
            function callback(results, status) {
                $('#restaurant').empty();
                for (var n = 0; n < results.length; n++) {
                    var restaurant = `<div><span class="span-name">${results[n].name}</span>  <span class="span-location">${results[n].vicinity}</span></div>`;
                    $('#restaurant').append(restaurant);
                }

                if (status === google.maps.places.PlacesServiceStatus.OK) {

                    for (var i = 0; i < results.length; i++) {
                        markers.push(createMarker(results[i]))
                    }
                }
            }

            // function สร้าง Marker ร้านอาหาร
            function createMarker(place) {
                var placeLoc = place.geometry.location

                var marker = new google.maps.Marker({
                    position: place.geometry.location,
                    map: map
                });
                google.maps.event.addListener(marker, 'rightclick', function () {
                    infowindow.setContent(place.name);
                    infowindow.open(map, this);
                });
                return marker
            }

            // function ลบ Marker เดิม
            function clearResults(markers) {
                for (var m in markers) {
                    markers[m].setMap(null)
                }
                markers = []
            }
            google.maps.event.addDomListener(window, 'load', initialize)
        } catch (error) {
            console.log(error)
        }
    </script>

    <style>
        /* Respond หน้าปกติ */
        html,
        body,
        #map {
            height: 100%;
            margin: 0px;
            padding: 0px;
        }

        #form-group {
            position: absolute;
            top: 10px;
            left: 10px;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border-radius: 5px;
            text-align: center;
            font-size: 'Roboto', 'sans-serif';
            line-height: 30px;
            padding-left: 10px;
            width: 600px;
        }

        #table {
            position: absolute;
            top: 70px;
            left: 10px;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border-radius: 5px;
            text-align: center;
            font-size: 'Roboto', 'sans-serif';
            line-height: 30px;
            padding-left: 10px;
            height: 500px;
            width: 600px;
            overflow-y: scroll;
            text-align: left;
            border: 5px solid #fff;
        }

        #search_input {
            width: 500px;
            border-radius: 5px;
            border: none;
        }

        .span-name {
            font-size: 12.5px;
            font-weight: bold;
        }

        .span-location {
            text-overflow: clip;
            font-weight: lighter;
            overflow: hidden;
            white-space: nowrap;
        }

        /* Respond หน้า 1440px */
        @media only screen and (max-width: 1440px) {
            #search_input {
                width: 200px;
            }

            #form-group {
                width: 300px;
            }

            #table {
                height: 650px;
                width: 300px;
            }

            .span-location {
                font-size: 11px;
                text-overflow: clip;
                overflow: hidden;
                white-space: nowrap;
            }

            .span-name {
                font-size: 12.5px;
            }
        }

        /* Respond หน้า 1024px */
        @media only screen and (max-width: 1024px) {
            #search_input {
                width: 150px;
                border-radius: 5px;
            }

            #form-group {
                width: 250px;
            }

            #table {
                height: 550px;
                width: 250px;
                overflow-y: scroll;
                text-align: left;
            }

            .span-location {
                font-size: 10px;
                text-overflow: clip;
                font-weight: lighter;
                overflow: hidden;
                white-space: nowrap;
            }

            .span-name {
                font-size: 11px;
                font-weight: bold;
            }
        }

        /* Respond หน้า 768px */
        @media only screen and (max-width: 768px) {
            #search_input {
                width: 100px;
                border-radius: 5px;
                border: none;
            }

            #form-group {
                width: 200px;
            }

            #table {
                height: 350px;
                width: 200px;
                overflow-y: scroll;
                text-align: left;
                border: 5px solid #fff;
            }

            .span-location {
                font-size: 10px;
                text-overflow: clip;
                font-weight: lighter;
                overflow: hidden;
                white-space: nowrap;
            }

            .span-name {
                font-size: 11px;
                font-weight: bold;
            }
        }
    </style>
    <!-- ################################################################################ -->
</head>

</html>
